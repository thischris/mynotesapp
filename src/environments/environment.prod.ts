export const environment = {
  production: true,

  // URL of prod API
  apiUrl: 'http://localhost:3000'
};
