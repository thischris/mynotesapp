import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'environments/environment';
import {Note} from './note';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class NoteService {

  constructor(private http: HttpClient) {
  }

  private notesUrl = environment.apiUrl;

  /** GET: get notes from the server */
  getNotes(): Observable<Note[]> {
    return this.http.get<Note[]>(this.notesUrl + '/notes')
      .pipe(
        tap(notes => console.log(`fetched notes`)),
        catchError(this.handleError('getNotes', []))
      );
  }
  /** POST: add a new note to the server */
  addNote (newNote: Note): Observable<Note> {
    return this.http.post<Note>(this.notesUrl + '/notes', newNote, httpOptions).pipe(
      tap((note: Note) => console.log(`added note with id=${note.id}`)),
      catchError(this.handleError<Note>('addNote'))
    );
  }
  /** DELETE: delete the note from the server */
  deleteNote (note: Note | number): Observable<Note> {
    const id = typeof note === 'number' ? note : note.id;
    const url = this.notesUrl + '/notes/' + id;
    return this.http.delete<Note>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted note id=${id}`)),
      catchError(this.handleError<Note>('deleteNote'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.error(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
