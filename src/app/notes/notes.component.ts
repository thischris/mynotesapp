import {Component, OnInit} from '@angular/core';
import {Note} from '../note';
import {NoteService} from '../note.service';

@Component({
  selector: 'app-notes',
  providers: [NoteService],
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  titleModel: string;
  contentModel: string;
  notes: Note[];

  constructor(private noteService: NoteService) {
    this.titleModel = '';
    this.contentModel = '';
  }

  getNotes(): void {
    this.noteService.getNotes()
      .subscribe(notes => this.notes = notes);
  }

  ngOnInit() {
    this.getNotes();
  }

  createNotes() {

    const newNote: Note = {
      title: this.titleModel,
      content: this.contentModel,
      id: (new Date()).getTime()
    };
    if (!newNote) { return; }

    this.noteService.addNote(newNote as Note)
      .subscribe(newNote => {
        this.notes.push(newNote);
      });
    this.titleModel = '';
    this.contentModel = '';
  }

  deleteNote(note: Note): void {
    this.notes = this.notes.filter(n => n !== note);
    this.noteService.deleteNote(note).subscribe();
  }
}
